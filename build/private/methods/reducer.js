"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.reducer = void 0;
const errors_1 = require("../../private/errors");
const reducer = (from, values) => (acc, key) => {
    const validator = values[key];
    const value = from[key];
    if (!validator) {
        throw new Error(errors_1.errors.NO_VALIDATOR(key));
    }
    if (value === undefined) {
        if (!validator.default) {
            throw new Error(errors_1.errors.NO_OPTIONS(key, validator.type));
        }
        acc[key] = validator.default;
    }
    else {
        if (!validator.validator(value)) {
            throw new Error(errors_1.errors.NOT_VALID(key, validator.type));
        }
        acc[key] = validator.converter(value);
    }
    return acc;
};
exports.reducer = reducer;
//# sourceMappingURL=reducer.js.map