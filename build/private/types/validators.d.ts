import { Config } from '../../public/types';
import { Spec } from './spec';
export declare type Validator<T> = {
    type: string;
    validator: (value: string | undefined) => boolean;
    converter: (value: string) => T;
};
export declare type ParticularValidator<T> = (config?: Config<T>) => Spec<T>;
//# sourceMappingURL=validators.d.ts.map