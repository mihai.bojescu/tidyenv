import { Spec, Validator } from '../../private/types';
import { Config } from './config';
export declare type GenericValidator = <T>(validator: Validator<T>) => (config?: Config<T>) => Spec<T>;
//# sourceMappingURL=validators.d.ts.map