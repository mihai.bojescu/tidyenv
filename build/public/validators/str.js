"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.str = void 0;
const generic_1 = require("./generic");
const str = (config) => generic_1.generic({
    type: 'string',
    validator: (value) => typeof value === 'string',
    converter: (value) => value
})(config);
exports.str = str;
//# sourceMappingURL=str.js.map