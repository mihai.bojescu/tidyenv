import { Spec, Validator } from '../../private/types'
import { Config } from './config'

export type GenericValidator = <T>(validator: Validator<T>) => (config?: Config<T>) => Spec<T>;
