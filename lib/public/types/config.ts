export type Config<K> = {
  default?: K;
};
