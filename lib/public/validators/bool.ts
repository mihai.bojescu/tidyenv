import { ParticularValidator } from '../../private/types'
import { generic } from './generic'

export const bool: ParticularValidator<boolean> = (config) =>
  generic({
    type: 'boolean',
    validator: (value) =>
      (typeof value === 'string' && ['true', 'false', '1', '0', ''].includes(value.toLocaleLowerCase())) ||
      (value !== undefined && !Number.isNaN(+value) && (+value === 0 || +value === 1)),
    converter: (value) =>
      ((['true', '1'].includes(value.toLocaleLowerCase()) || +value === 1) && true) ||
      ((['false', '0', ''].includes(value.toLocaleLowerCase()) || +value === 0) && false)
  })(config)
