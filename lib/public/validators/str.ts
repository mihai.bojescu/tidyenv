import { ParticularValidator } from '../../private/types'
import { generic } from './generic'

export const str: ParticularValidator<string> = (config) =>
  generic({
    type: 'string',
    validator: (value) => typeof value === 'string',
    converter: (value) => value
  })(config)
