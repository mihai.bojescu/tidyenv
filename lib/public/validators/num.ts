import { ParticularValidator } from '../../private/types'
import { generic } from './generic'

export const num: ParticularValidator<number> = (config) =>
  generic({
    type: 'number',
    validator: (value) => value !== undefined && !Number.isNaN(+value),
    converter: (value) => Number(value)
  })(config)
