import { keysOf, reducer } from '../../private/methods'
import { Specs } from '../../private/types'

export const process = <T extends Record<string, unknown>>(
  from: Record<string, string | undefined>,
  values: Specs<T>
): Readonly<T> => {
  const entries = keysOf(values)
  const unfrozenResult = entries.reduce<T>(reducer(from, values), {} as T)

  return Object.freeze(unfrozenResult)
}
