import { KeysOf } from '../../private/types'

export const keysOf: KeysOf = (obj) => Object.keys(obj)
