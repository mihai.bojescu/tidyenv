import { Specs } from '../../private/types'

export type KeysOf = <T extends Record<string, unknown>>(obj: T) => (keyof T)[];

export type Reducer = <T>(from: Record<string, string | undefined>, values: Specs<T>) => (acc: T, key: keyof T) => T;

export type Tidy = <T>(from: Record<string, string | undefined>, values: Specs<T>) => Readonly<T>;
