import { Config } from '../../public/types'
import { Validator } from './validators'

export type Spec<K> = Config<K> & Validator<K>;

export type Specs<T> = {
  [K in keyof T]: Spec<T[K]>;
};
