export const NO_VALIDATOR = (entry: string) => `Error while reading the '${entry}' value': Validator not valid.`;
export const NO_OPTIONS = (entry: string, type: string) =>
  `Error while reading the '${entry}' value with type '${type}': No default or value provided.`;
export const NOT_VALID = (entry: string, type: string) =>
  `Error while reading the '${entry}' value with type '${type}': Types don't match.`;
