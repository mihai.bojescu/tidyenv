# TidyEnv

Tidy up your environment variables, the easy way

## Features

- Assert the types of your environment variables in order to guard against runtime issues
- Build custom validators
- Add default values for environment variables

## Notes

This project in it's _really_ early stages. More features should be available in the next releases.

## Building

Run `npm run build`

## Examples

### Validate environment variables

If you want to validate your environment variables, you can do something like this:

```typescript
import process from 'process'
import { tidyEnv } from 'tidyenv'

export const env = tidyEnv.process(
  process.env,
  {
    PORT: tidyEnv.num({ default: 3000 }),
    DB_URL: tidyEnv.str()
  }
)
```

### Create custom validators

```typescript
import { tidyEnv } from 'tidyenv'

export const emailValidator = tidyEnv.generic({
  type: 'email',
  validator: value => value !== undefined && /\w+@\w+\.com/g.test(value),
  converter: value => value
})

export const env = tidyEnv.process(
  process.env,
  {
    ADMIN_EMAIL: emailValidator({ default: 'user@domain.com' }),
  }
)
```
